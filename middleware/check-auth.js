const tokenService = require('../services/tokenService');

module.exports = (req, res, next) => {
  try{
      const token = req.headers.authorization;
      const decodeToken = tokenService.decodeToken(token);
      if(decodeToken){
        req.userData = decodeToken;
        next();
      } else {
          res.status(401).json({message: "Autenticazione fallita", err})
      }
  } catch (err) {
      res.status(401).json({message: "Autenticazione fallita", err})
  }
};
