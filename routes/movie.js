const express = require('express');
const router = express.Router();
const movieController = require('../controller/movie');

router.post('/:actorId([0-9a-fA-F]{24})/movies', movieController.store);

router.patch('/:actorId([0-9a-fA-F]{24})/movies/:id([0-9a-fA-F]{24})', movieController.lightUpdate);

router.delete('/:actorId([0-9a-fA-F]{24})/movies/:id([0-9a-fA-F]{24})', movieController.destroy);

module.exports = router;
