const express = require('express');
const router = express.Router();
const userController = require('../controller/user');



router.get('', userController.index);

router.post('', userController.store);

router.get('/:id([0-9a-fA-F]{24})', userController.show);

router.put('/:id([0-9a-fA-F]{24})', userController.update);

router.patch('/:id([0-9a-fA-F]{24})', userController.lightUpdate);

router.delete('/:id([0-9a-fA-F]{24})', userController.destroy);

router.get('/logged', userController.logged);

module.exports = router;
