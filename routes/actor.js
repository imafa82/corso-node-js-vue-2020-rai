const express = require('express');
const router = express.Router();
const actorController = require('../controller/actor');
const movieRoutes = require('./movie');


router.get('', actorController.index);

router.post('', actorController.store);

router.get('/:id([0-9a-fA-F]{24})', actorController.show);

router.put('/:id([0-9a-fA-F]{24})', actorController.update);

router.patch('/:id([0-9a-fA-F]{24})', actorController.lightUpdate);

router.delete('/:id([0-9a-fA-F]{24})', actorController.destroy);

router.use('', movieRoutes);

module.exports = router;
