const express = require('express');
const router = express.Router();
const actorsRoutes = require('./actor');
const usersRoutes = require('./user');

router.use('/users', usersRoutes);
router.use('/actors', actorsRoutes);

module.exports = router;
