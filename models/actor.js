const mongoose = require('mongoose');

const Movie= {
    title: {type: String, required: true},
    year: {type: Number},
    path: {type: String}
}

const userSchema = mongoose.Schema({
    name: {type: String, required: true},
    surname: {type: String, required: true},
    year: {type: String},
    path: {type: String},
    movies: {type: [Movie], required: true}
});


module.exports = mongoose.model('Actor', userSchema);
