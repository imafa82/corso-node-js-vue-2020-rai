const mongo = require('mongodb');
const mongoClient = mongo.MongoClient;
const url = "mongodb://localhost:27017/";

let connection = null;

const init = (resolve) => {
    mongoClient.connect(url, function (err, db) {
        if(err) throw err;
        connection = db.db('lesson3');
        resolve(connection);
    })
};

module.exports.get = new Promise((resolve, reject) => {
   if(connection){
       resolve(connection)
   } else {
       init(resolve)
   }
});


module.exports.select = name => {
    return new Promise(resolve => {
        this.get.then(db => {
            resolve(db.collection(name));
        })
    })
};
