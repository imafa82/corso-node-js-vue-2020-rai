const express = require('express');
const fs = require('fs');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const http = require('http').createServer(app);
const socketService= require('./services/socketService');
socketService.socket.init(http);
const User = require('./models/user');
const cors = require('cors');
http.listen(2000);
const checkAuth = require('./middleware/check-auth');
const authRoutes = require('./routes/authRoute');
const loginRoutes = require('./routes/login');
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, If-None-Match ");
    res.header("Access-Control-Allow-Methods", "GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS");
    next();
});
app.use(cors({
    exposedHeaders: ['Authorization']
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static('public'));
app.use('/api/login', loginRoutes);
app.use('/api', checkAuth, authRoutes);

mongoose.connect("mongodb://localhost:27017/lesson3", {useNewUrlParser: true}).then(() => {
    console.log('connect db');
    User.find().then(res => {
        console.log(res);
    })
}).catch(err => console.log('errore connessione db', err));

app.all("*", (req, res) => {
   try {
       res.sendFile('public/index.html', {root: __dirname});
   } catch (e) {
       res.status(404).json({message: 'Non è stato trovato il path in public'})
   }
});
// app.get('/url', (req, res)=> {
//     res.send(req.query);
// });
//
// app.get('/actors', (req, res) => {
//     socketService.socket.connection.emit('message', 'Richiesta lista attori');
//     console.log('dentro actors')
//    let callback = response => res.send(response);
//     readFileActors(callback);
// });
//
// app.get('/actors/:id([0-9]+)', (req, res) => {
//     const callback = response => {
//       let actor = response.find(act => act.id === +req.params.id);
//       if(!actor){
//           res.status(404);
//       }
//       res.send(actor)
//     };
//     readFileActors(callback);
// });
//
// function readFileActors(callback) {
//     const read = fs.createReadStream('./actors.json');
//     let response = '';
//     read.on('data', data => {
//         response += data.toString();
//     });
//     read.on('end', () => {
//         callback && callback(JSON.parse(response));
//     })
// }
