const _ = require('lodash');
const fs = require('fs');

fs.exists('actors.json', r => {
    if(!r){
        const actors = [
            {name: 'Lino Banfi', year: 1945, id:1},
            {name: 'Adriano Celentano', year: 1952, id:2},
            {name: 'Bud Spencer', year: 1951, id:3},
            {name: 'Diego Abatantuono', year: 1946, id:4}
        ];
        fs.writeFile('actors.json', JSON.stringify(actors), {encoding: 'utf8'}, err => {
            console.log(err);
        });
    }
});


let newObj = {
    name: 'Massimiliano',
    surname: 'Salerno',
    array: [5, 6, 8, 9],
    print: function () {
        console.log(`${this.name} ${this.surname}`);
        setTimeout(() => {
            console.log('dentro set Timeout');
            console.log(`Il nome è ${this.name}`);
        }, 2000)
    },
    printArray: function () {
        this.array.forEach(function (ele) {
            console.log(ele, this.name)
        })
    }
};

newObj.print();
newObj.printArray();
console.log('ci siamo');


var sum = (num1, num2) => num1 + num2;
var sum2 = num1 => num1 * 2;
var info = () => console.log('info');

console.log(sum(5, 2));
