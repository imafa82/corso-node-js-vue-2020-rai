const jwt = require('jsonwebtoken');

const secret = 'password da scegliere';


exports.createJWTToken = ({_id}) => {
    console.log(_id);
    console.log(jwt.sign(
        {_id},
        secret,
        {expiresIn: '1 days'}
    ));
    return jwt.sign(
        {_id},
        secret,
        {expiresIn: '1 days'}
    )
};


exports.decodeToken = token => {
    try {
        return jwt.verify(token, secret);
    } catch (e) {
        return false
    }
}
