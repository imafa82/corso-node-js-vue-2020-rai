const tokenService = require('./tokenService');

class SocketConnect {
    constructor(){
        this.connection = null;
    }
    init(http){
        const io = require('socket.io').listen(http);
        this.connection = io.sockets.on('connection', function (socket) {
            socket.on('actorsRoom', function ({token}) {
                if(token && tokenService.decodeToken(token)){
                    console.log('actors room');
                    socket.join('actors');
                } else {
                    console.log('senza autenticazione');
                }

            })
        })
    }
}

exports.socket = new SocketConnect();
