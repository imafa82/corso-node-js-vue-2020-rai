const ObjectId = require('mongodb').ObjectID;
const Actor = require('../models/actor');
const socketService = require('../services/socketService');


exports.index = (req, res) => {
    Actor.find({}).then(actors => {
        res.send(actors);
    }, err => {
        res.status(400).json({message: 'Errore query db', err});
    })
};

exports.store = (req, res) => {
       const actor = new Actor(req.body);
        actor.save().then(result => {
            socketService.socket.connection.to('actors').emit('addActor', result);
            res.status(201).json(result);
        })
            .catch(err => res.status(400).json({message: 'Errore query db', err}))
};


exports.show = (req, res) => {
    try {
        Actor.findOne({_id : ObjectId(req.params.id)}).then(result => {
            result?
                res.status(200).json(result) :
                res.status(400).json({message: 'Attore non presente'});
        }).catch(err => res.status(400).json({message: 'Errore query db', err}));
    } catch (err) {
        res.status(400).json({message: 'Errore query db', err});
    }
};

exports.update = (req, res) => {
    try {
        Actor.replaceOne({_id : ObjectId(req.params.id)}, req.body).then(result => {
            res.status(200).json(result);
        });
    } catch (err) {
        res.status(400).json({message: 'Errore query db', err});
    }
}

exports.lightUpdate = (req, res) => {
    try {
        Actor.updateOne({_id : ObjectId(req.params.id)}, req.body).then(result => {
            res.status(200).json(result);
        });
    } catch (err) {
        res.status(400).json({message: 'Errore query db', err});
    }
}

exports.destroy = (req, res) => {
    Actor.remove({_id: ObjectId(req.params.id)}).then(result => {
        res.status(200).json(result);
    });
};
