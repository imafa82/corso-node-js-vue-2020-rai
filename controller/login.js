const User = require('../models/user');
const bcrypt = require('bcrypt');
const tokenService = require('../services/tokenService');


exports.login = (req, res) => {
    if(req.body.email && req.body.password){
            const {email} = req.body;
            try {
                User.findOne({email}, function (err, result) {
                    if(err) res.status(400).json({message: 'Errore query db', err});
                    result ?
                        controlPassword(req, res, result) :
                        res.status(401).json({message: 'Parametri di autenticazione errati'})
                })
            } catch (err) {
                res.status(400).json({message: 'Errore query db', err});
            }
    } else {
        res.status(400).json({message: 'Manca username o password'});
    }

};

function controlPassword(req, res, user) {
    bcrypt.compare(req.body.password, user.password).then(psw => {
        psw ?
            res.status(200).json({user, token: tokenService.createJWTToken(user)}) :
            res.status(401).json({message: 'Parametri di autenticazione errati'});
    })
}
