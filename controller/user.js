const ObjectId = require('mongodb').ObjectID;
const User = require('../models/user');
const bcrypt = require('bcrypt');



exports.index = (req, res) => {
    User.find({}).then(users => {
        res.send(users);
    }, err => {
        res.status(400).json({message: 'Errore query db', err});
    })
};

exports.store = (req, res) => {
    const {password} = req.body;
    bcrypt.hash(password, 10).then(password => {
       const user = new User({
           ...req.body,
           password
       });
        user.save().then(result => res.status(201).json(result))
            .catch(err => res.status(400).json({message: 'Errore query db', err}))
    });
};

exports.show = (req, res) => {
        try {
            User.findOne({_id : ObjectId(req.params.id)}).then(result => {
                result?
                    res.status(200).json(result) :
                    res.status(400).json({message: 'Utente non presente'});
            }).catch(err => res.status(400).json({message: 'Errore query db', err}));
        } catch (err) {
            res.status(400).json({message: 'Errore query db', err});
        }
};

exports.update = (req, res) => {
        try {
            User.replaceOne({_id : ObjectId(req.params.id)}, req.body).then(result => {
                res.status(200).json(result);
            });
        } catch (err) {
            res.status(400).json({message: 'Errore query db', err});
        }
}

exports.lightUpdate = (req, res) => {
        try {
            User.updateOne({_id : ObjectId(req.params.id)}, req.body).then(result => {
                res.status(200).json(result);
            });
        } catch (err) {
            res.status(400).json({message: 'Errore query db', err});
        }
}

exports.destroy = (req, res) => {
      User.remove({_id: ObjectId(req.params.id)}).then(result => {
          res.status(200).json(result);
      });
};

exports.logged = (req, res) => {
        try {
            User.findOne({_id: ObjectId(req.userData._id)}, {password: 0}).then(result => {
                result?
                    res.status(200).json(result) : res.status(404).json({message: 'Utente non trovato'});
            });
        } catch (err) {
            res.status(400).json({message: 'Errore query db', err});
        }
};
