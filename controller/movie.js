const ObjectId = require('mongodb').ObjectID;
const Actor = require('../models/actor');

exports.store = (req, res) => {
        const _id = req.params.actorId;
        console.log(_id);
        Actor.updateOne({_id}, {$push: {movies: req.body}}).then(result => {
            res.status(201).json(result);
        })
};


exports.lightUpdate = (req, res) => {
    const _id = req.params.actorId;
    console.log(_id);
    Actor.updateOne({_id, 'movies._id': req.params.id},
        {$set: {
            'movies.$.title': req.body.title,
            'movies.$.year': req.body.year,
            'movies.$.path': req.body.path,
        }}).then(result => {
        res.status(201).json(result);
    })
}

exports.destroy = (req, res) => {
    const _id = req.params.actorId;
    console.log(_id);
    Actor.updateOne({_id}, {$pull: {movies: {_id: req.params.id}}}).then(result => {
        res.status(201).json(result);
    })
};
